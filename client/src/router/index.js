import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home.vue";
import EstadisticaMes from "../views/EstadisticaMes.vue";
import Categoria from "../components/Categoria.vue";
import Usuario from "../components/Usuario.vue";
import Articulo from "../components/Articulo.vue";
import Cliente from "../components/persona/Cliente.vue";
import Proveedor from "../components/persona/Proveedor.vue";
import Ingreso from "../components/Ingreso.vue";
import Venta from "../components/Venta.vue";

import FechaVentas from "../components/layouts/FechaVentas.vue";
import FechaCompra from "../components/layouts/FechaIngreso.vue";

import Presentation from "../views/Presentation.vue";

import store from "../store";
import Login from "../components/log/Login.vue";

Vue.use(VueRouter);

const routes = [

  {
    path: "/",
    name: "home",
    component: Home,
    meta: {
      administrador: true,
      almacenero: true,
      vendedor: true,
    },
  },
  {
    path: "/estadistica",
    name: "EstadisticaMes",
    component: EstadisticaMes,
    meta: {
      administrador: true,
      almacenero: true,
      vendedor: true,
    },
  },
  {
    path: "/fechaVentas",
    name: "FechaVentas",
    component: FechaVentas,
    meta: {
      administrador: true,
      almacenero: true,
      vendedor: true,
    },
  },
  {
    path: "/fechaCompra",
    name: "FechaCompra",
    component: FechaCompra,
    meta: {
      administrador: true,
      almacenero: true,
      vendedor: true,
    },
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      log: true,
    },
  },
  {
    path: "/usuario",
    name: "usuario",
    component: Usuario,
    meta: {
      administrador: true,
    },
  },
  {
    path: "/categoria",
    name: "categoria",
    component: Categoria,
    meta: {
      administrador: true,
      almacenero: true,
    },
  },
  {
    path: "/articulo",
    name: "articulo",
    component: Articulo,
    meta: {
      administrador: true,
      almacenero: true,
    },
  },
  {
    path: "/cliente",
    name: "cliente",
    component: Cliente,
    meta: {
      administrador: true,
      vendedor: true,
    },
  },
  {
    path: "/proveedor",
    name: "proveedor",
    component: Proveedor,
    meta: {
      administrador: true,
      almacenero: true,
    },
  },
  {
    path: "/ingreso",
    name: "ingreso",
    component: Ingreso,
    meta: {
      administrador: true,
      almacenero: true,
    },
  },
  {
    path: "/venta",
    name: "venta",
    component: Venta,
    meta: {
      administrador: true,
      vendedor: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to, from, next) => {
  let user = store.state.usuario;

  if (to.matched.some(({ meta }) => meta.log)) {
    !user && to.name !== "login" ? next({ name: "login" }) : next();
  }

  if (user && user.rol === "Administrador") {
    if (to.matched.some(({ meta }) => meta.administrador)) {
      next();
    }
  }
  if (user && user.rol === "Vendedor") {
    if (to.matched.some(({ meta }) => meta.vendedor)) {
      next();
    }
  }
  if (user && user.rol === "Almacenero") {
    if (to.matched.some(({ meta }) => meta.almacenero)) {
      next();
    }
  }
});
export default router;
