import { Schema, model } from "mongoose";

const ventaSchema = new Schema({
  usuario: {
    type: Schema.ObjectId,
    ref: "usuario",
    require: true,
  },
  persona: {
    type: Schema.ObjectId,
    ref: "persona",
    require: true,
  },
  tipo_comprobante: {
    type: String,
    maxlength: 20,
    require: true,
  },
  serie_comprobante: {
    type: String,
    maxlength: 20,
  },
  num_comprobante: {
    type: String,
    require: true,
  },
  impuesto: {
    type: Number,
    require: true,
  },
  precioT: {
    type: Number,
    require: true,
  },
  precioSI: {
    type: Number,
    require: true,
  },
  valorI: {
    type: Number,
    require: true,
  },
  detalles: [
    {
      _id: {
        type: String,
        require: true,
      },
      articulo: {
        type: String,
        require: true,
      },
      cantidad: {
        type: Number,
        require: true,
      },
      precioU: {
        type: Number,
        require: true,
      },
    },
  ],
  estado: {
    type: Number,
    default: 1,
  },
  createAt: {
    type: Date,
    default: Date.now(),
  },
});

export default  model("venta", ventaSchema);

