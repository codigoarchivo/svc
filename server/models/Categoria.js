import { Schema, model } from "mongoose";

const categoriaSchema = new Schema({
  nombre: {
    type: String,
    maxlength: 50,
    unique: true,
    require: true,
  },
  descripcion: {
    type: String,
    maxlength: 255,
  },
  estado: {
    type: Number,
    default: 1,
  },
  createAt: {
    type: Date,
    default: Date.now(),
  },
});

export default model("categoria", categoriaSchema);
