import { Schema, model } from "mongoose";

const articuloEsquema = new Schema({
  categoria: {
    type: Schema.ObjectId,
    ref: "categoria",
  },
  codigo: {
    type: String,
    maxlength: 64,
  },
  nombre: {
    type: String,
    maxlength: 50,
    unique: true,
    require: true,
  },
  descripcion: {
    type: String,
    maxlength: 255,
  },
  precio_venta: {
    type: Number,
    require: true,
  },
  stock: {
    type: Number,
    require: true,
  },
  estado: {
    type: Number,
    default: 1,
  },
  createAt: {
    type: Date,
    default: Date.now(),
  },
});

export default model("articulo", articuloEsquema);
