import { Schema, model } from "mongoose";

const personaSchema = new Schema({
  tipo_persona: {
    type: String,
    maxlength: 20,
    require: true,
  },
  nombre: {
    type: String,
    maxlength: 50,
    require: true,
  },
  tipo_documento: {
    type: String,
    maxlength: 20,
  },
  num_documento: {
    type: String,
    maxlength: 20,
  },
  direccion: {
    type: String,
    maxlength: 70,
  },
  telefono: {
    type: String,
    maxlength: 20,
  },
  email: {
    type: String,
    maxlength: 64,
    unique: true,
  },
  estado: {
    type: Number,
    default: 1,
  },
  createAt: {
    type: Date,
    default: Date.now(),
  },
});

export default model("persona", personaSchema);
