import { Schema, model } from "mongoose";

const usuarioSchema = new Schema({
  imagen: {
    type: String,
    require: true,
  },
  rol: {
    type: String,
    require: true,
  },
  nombre: {
    type: String,
    maxlength: 50,
    require: true,
    unique: true,
  },
  tipo_documento: {
    type: String,
  },
  num_documento: {
    type: String,
    maxlength: 20,
  },
  direccion: {
    type: String,
    maxlength: 70,
  },
  telefono: {
    type: String,
    maxlength: 20,
  },
  email: {
    type: String,
    require: true,
    unique: true,
  },
  password: {
    type: String,
    maxlength: 64,
    require: true,
  },
  estado: {
    type: Number,
    default: 1,
  },
  createAt: {
    type: Date,
    default: Date.now(),
  },
});

export default model("usuario", usuarioSchema);
