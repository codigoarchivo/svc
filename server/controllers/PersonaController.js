import Persona from "../models/Persona";

export const listClientes = async (req, res, next) => {
  try {
    const valor = await req.query.valor;
    const reg = await Persona.find(
      {
        $or: [
          { nombre: new RegExp(valor, "i") },
          { email: new RegExp(valor, "i") },
        ],
        // filtrado cuando es el tipo de cliente
        tipo_persona: "Cliente",
      },
      { createAt: 0 }
    ).sort({ createAt: -1 });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const listProveedores = async (req, res, next) => {
  try {
    const valor = await req.query.valor;
    const reg = await Persona.find(
      {
        $or: [
          { nombre: new RegExp(valor, "i") },
          { email: new RegExp(valor, "i") },
        ],
        // filtrado cuando es el tipo de Proveedores
        tipo_persona: "Proveedor",
      },
      { createAt: 0 }
    ).sort({ createAt: -1 });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const add = async (req, res, next) => {
  try {
    const reg = await Persona.create(req.body);
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const query = async (req, res, next) => {
  try {
    const reg = await Persona.findOne({ _id: req.query._id });
    if (!reg) {
      res.status(404).send({
        message: "No se encuentra",
      });
    } else {
      res.status(200).json(reg);
    }
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const list = async (req, res, next) => {
  try {
    const valor = await req.query.valor;
    const reg = await Persona.find(
      {
        $or: [
          { nombre: new RegExp(valor, "i") },
          { email: new RegExp(valor, "i") },
        ],
      },
      { createAt: 0 }
    ).sort({ createAt: -1 });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const update = async (req, res, next) => {
  try {
    const reg = await Persona.findByIdAndUpdate(
      { _id: req.body._id },
      {
        tipo_persona: req.body.tipo_persona,
        nombre: req.body.nombre,
        tipo_documento: req.body.tipo_documento,
        num_documento: req.body.num_documento,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        email: req.body.email,
      }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const remove = async (req, res, next) => {
  try {
    const reg = await Persona.findByIdAndDelete({ _id: req.body._id });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const activate = async (req, res, next) => {
  try {
    const reg = await Persona.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 1 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const desactivate = async (req, res, next) => {
  try {
    const reg = await Persona.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 0 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};
