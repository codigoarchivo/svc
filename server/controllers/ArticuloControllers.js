import Articulo from "../models/Articulo";
import becrypt from "bcrypt";

export const add = async (req, res, next) => {
  try {
    req.body.password = becrypt.hash(req.body.password, 10);
    const reg = await Articulo.create(req.body);
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const query = async (req, res, next) => {
  try {
    const reg = await Articulo.findOne({
      _id: req.query._id,
    }).populate("categoria", { nombre: 1 });

    if (!reg) {
      res.status(404).send({
        message: "No se Encuentra",
      });
    } else {
      res.status(200).json(reg);
    }
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const queryCodigo = async (req, res, next) => {
  try {
    const reg = await Articulo.findOne({
      codigo: req.query.codigo,
    }).populate("categoria");

    if (!reg) {
      res.status(404).send({
        message: "No se Encuentra",
      });
    } else {
      res.status(200).json(reg);
    }
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const list = async (req, res, next) => {
  try {
    let valor = req.query.valor;
    const reg = await Articulo.find(
      {
        $or: [
          { nombre: new RegExp(valor, "i") },
          { descripcion: new RegExp(valor, "i") },
        ],
      },
      { createdAt: 0 }
    )
      //*populate 1 parametro hace referencia modelo categoria
      //*populate 2 parametro hace referencia que nos mueste en este ejemplo el nombre
      .populate("categoria")
      .sort({ createdAt: -1 });

    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const update = async (req, res, next) => {
  try {
    const reg = await Articulo.findByIdAndUpdate(
      { _id: req.body._id },
      {
        //propiedad Categoria
        categoria: req.body.categoria,
        // modelos articulos
        codigo: req.body.codigo,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        precio_venta: req.body.precio_venta,
        stock: req.body.stock,
      }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const remove = async (req, res, next) => {
  try {
    const reg = await Articulo.findByIdAndDelete({
      _id: req.body._id,
    });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const activate = async (req, res, next) => {
  try {
    const reg = await Articulo.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 1 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const desactivate = async (req, res, next) => {
  try {
    const reg = await Articulo.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 0 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};
