import Categoria from "../models/Categoria";

export const add = async (req, res, next) => {
  try {
    const reg = await Categoria.create(req.body);
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const query = async (req, res, next) => {
  try {
    const reg = await Categoria.findOne({ _id: req.query._id });
    if (!reg) {
      res.status(404).send({
        message: "No se Encuentra",
      });
    } else {
      res.status(200).json(reg);
    }
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const list = async (req, res, next) => {
  try {
    // El objeto RegExp se utiliza para hacer coincidir texto con un patrón.
    //  $or significa que puede buscar por los dos
    let valor = req.query.valor;
    const reg = await Categoria.find(
      // parametro indicar la busqueda
      {
        $or: [
          { nombre: new RegExp(valor, "i") },
          { descripcion: new RegExp(valor, "i") },
        ],
      },
      // parametro las propiedades filtradas que quieres o no que aparezca
      { createAt: 0 }
      // sort es para realizar en este caso las busquedas decendentes
      // colocamos createAt porque los documentos que se crean recientemente van a aparecer primero
    ).sort({ createAt: -1 });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const update = async (req, res, next) => {
   try {
      const reg = await Categoria.findByIdAndUpdate(
        { _id: req.body._id },
        {
          nombre: req.body.nombre,
          descripcion: req.body.descripcion,
        }
      );
      res.status(200).json(reg);
    } catch (error) {
      res.status(500).send({
        message: "Ocurrio un Error",
      });
      next(error);
    }
};

export const remove = async (req, res, next) => {
  try {
    const reg = await Categoria.findByIdAndDelete({
      _id: req.body._id,
    });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const activate = async (req, res, next) => {
  try {
    const reg = await Categoria.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 1 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const desactivate = async (req, res, next) => {
  try {
    const reg = await Categoria.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 0 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};
