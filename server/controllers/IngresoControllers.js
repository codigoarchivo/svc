import Articulo from "../models/Articulo";
import Ingreso from "../models/Ingreso";

/*  stock Articulo */
const aumentarStock = async (id_articulo, cantidad) => {
  const { stock } = await Articulo.findOne({ _id: id_articulo });
  await Articulo.findByIdAndUpdate(
    { _id: id_articulo },
    { stock: parseInt(stock) + parseInt(cantidad) }
  );
};

const disminuirStock = async (id_articulo, cantidad) => {
  const { stock } = await Articulo.findOne({ _id: id_articulo });
  await Articulo.findByIdAndUpdate(
    { _id: id_articulo },
    { stock: parseInt(stock) - parseInt(cantidad) }
  );
};
/*  Ingreso  */
export const add = async (req, res, next) => {
  try {
    const reg = await Ingreso.create(req.body);

    //? actualizar el stock
    req.body.detalles.map((x) => {
      aumentarStock(x._id, x.cantidad);
    });

    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const query = async (req, res, next) => {
  try {
    const reg = await Ingreso.findOne({
      _id: req.query._id,
    })
      .populate("usuario")
      .populate("persona");
    if (!reg) {
      res.status(404).send({
        message: "No se Encuentra",
      });
    } else {
      res.status(200).json(reg);
    }
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const list = async (req, res, next) => {
  try {
    const valor = req.query.valor;
    const reg = await Ingreso.find({
      $or: [
        { num_comprobante: new RegExp(valor, "i") },
        { serie_comprobante: new RegExp(valor, "i") },
      ],
    })
      .populate("usuario")
      .populate("persona")
      .sort({ createAt: -1 });

    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const activate = async (req, res, next) => {
  try {
    const reg = await Ingreso.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 1 }
    );

    //? actualizar el stock
    reg.detalles.map(function (x) {
      aumentarStock(x._id, x.cantidad);
    });

    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const desactivate = async (req, res, next) => {
  try {
    const reg = await Ingreso.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 0 }
    );

    //? disminuir el stock
    reg.detalles.map(function (x) {
      disminuirStock(x._id, x.cantidad);
    });

    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const grafico12meses = async (req, res, next) => {
  try {
    const reg = await Ingreso.aggregate([
      {
        $group: {
          _id: { mes: { $month: "$createAt" }, year: { $year: "$createAt" } },
          total: { $sum: "$total" },
          numero: { $sum: 1 },
        },
      },
      { $sort: { "_id.year": -1, "_id.mes": -1 } },
    ]).limit(12);
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const consultaFechas = async (req, res, next) => {
  try {
    const start = req.query.start;
    const end = req.query.end;
    const reg = await Ingreso.find({
      createAt: { $gte: start, $lt: end },
    })
      .populate("usuario")
      .populate("persona")
      .sort({ createAt: -1 });

    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};
