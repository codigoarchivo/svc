import bcrypt from "bcrypt";
import Usuario from "../models/Usuario";
import { encode } from "../services/token";

export const login = async (req, res, next) => {
  try {
    const user = await Usuario.findOne({
      email: req.body.email,
      estado: 1,
    });
  
    
    if (!user) {
      return res.status(404).send({
        message: "No se encuentra",
      });
    }

    const match = await bcrypt.compare(req.body.password, user.password);

    if (!match) {
      return res.status(404).send({
        message: "Passworwd Incorrecto",
      });
    }

    const tokenReturn = await encode(
      user._id,
      user.rol,
      user.email,
      user.nombre,
      user.imagen
    );

    res.status(200).json({ user, tokenReturn });
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const perfil = async (req, res, next) => {
  try {
    const reg = await Usuario.updateOne(
      { _id: req.body._id },
      {
        $set: { imagen: req.body.imagen },
      }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const add = async (req, res, next) => {
  try {
    req.body.password = await bcrypt.hash(req.body.password, 10);
    const reg = await Usuario.create(req.body);
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const query = async (req, res, next) => {
  try {
    const reg = await Usuario.findOne({ _id: req.query._id });
    if (!reg) {
      return res.status(404).send({
        message: "No se Encuentra",
      });
    } else {
      res.status(200).json(reg);
    }
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const list = async (req, res, next) => {
  try {
    // El objeto RegExp se utiliza para hacer coincidir texto con un patrón.
    //  $or significa que puede buscar por los dos
    let valor = req.query.valor;
    const reg = await Usuario.find(
      {
        $or: [
          { nombre: new RegExp(valor, "i") },
          { email: new RegExp(valor, "i") },
        ],
      },
      { createAt: 0 }
    ).sort({ createAt: -1 });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un error",
    });
    next(error);
  }
};

export const update = async (req, res, next) => {
  try {
    const { password } = await Usuario.findOne({ _id: req.body._id });

    if (req.body.password != password) {
      req.body.password = await bcrypt.hash(req.body.password, 10);
    }

    const reg = await Usuario.findByIdAndUpdate(
      { _id: req.body._id },
      {
        // modelos usuario
        rol: req.body.rol,
        nombre: req.body.nombre,
        tipo_documento: req.body.tipo_documento,
        num_documento: req.body.num_documento,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        email: req.body.email,
        password: req.body.password,
      }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const remove = async (req, res, next) => {
  try {
    const reg = await Usuario.findByIdAndDelete({
      _id: req.body._id,
    });
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const activate = async (req, res, next) => {
  try {
    const reg = await Usuario.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 1 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};

export const desactivate = async (req, res, next) => {
  try {
    const reg = await Usuario.findByIdAndUpdate(
      { _id: req.body._id },
      { estado: 0 }
    );
    res.status(200).json(reg);
  } catch (error) {
    res.status(500).send({
      message: "Ocurrio un Error",
    });
    next(error);
  }
};
