import { Router } from "express";
import { veryfiGrocer } from "../middlewares/auth";

import {
  add,
  query,
  list,
  update,
  remove,
  activate,
  desactivate,
} from "../controllers/CategoriaController";

const router = Router();

router.post("/add", veryfiGrocer, add);
router.get("/query", veryfiGrocer, query);
router.get("/list", veryfiGrocer, list);
router.put("/update", veryfiGrocer, update);
router.delete("/remove", veryfiGrocer, remove);
router.put("/activate", veryfiGrocer, activate);
router.put("/desactivate", veryfiGrocer, desactivate);

export default router;
