import { Router } from "express";
import { veryfiAdmin } from "../middlewares/auth";

import {
  add,
  query,
  list,
  update,
  remove,
  activate,
  desactivate,
  perfil,
  login,
} from "../controllers/UsuarioControllers";

const router = Router();

router.post("/add", veryfiAdmin, add);
router.get("/query", veryfiAdmin, query);
router.get("/list", veryfiAdmin, list);
router.put("/update", veryfiAdmin, update);
router.delete("/remove", veryfiAdmin, remove);
router.put("/activate", veryfiAdmin, activate);
router.put("/desactivate", veryfiAdmin, desactivate);

router.put("/perfil", veryfiAdmin, perfil);
router.post("/login", login);

export default router;
