import { Router } from "express";
import { veryfiGrocer, veryfiUser } from "../middlewares/auth";

import {
  add,
  query,
  list,
  activate,
  desactivate,
  grafico12meses,
  consultaFechas,
} from "../controllers/VentasControllers";

const routes = Router();

routes.post("/add", veryfiGrocer, add);
routes.get("/query", veryfiGrocer, query);
routes.get("/list", veryfiGrocer, list);
routes.put("/activate", veryfiGrocer, activate);
routes.put("/desactivate", veryfiGrocer, desactivate);

routes.get("/grafico12meses", veryfiUser, grafico12meses);
routes.get("/consultaFechas", veryfiUser, consultaFechas);

export default routes;
