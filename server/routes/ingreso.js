import { Router } from "express";
import { veryfiGrocer, veryfiUser } from "../middlewares/auth";

import {
  add,
  query,
  list,
  activate,
  desactivate,
  grafico12meses,
  consultaFechas,
} from "../controllers/IngresoControllers";

const router = Router();

router.post("/add", veryfiGrocer, add);
router.get("/query", veryfiGrocer, query);
router.get("/list", veryfiGrocer, list);
router.put("/activate", veryfiGrocer, activate);
router.put("/desactivate", veryfiGrocer, desactivate);

router.get("/grafico12meses", veryfiUser, grafico12meses);
router.get("/consultaFechas", veryfiUser, consultaFechas);

export default router;
