import { Router } from "express";

import {
  veryfiUser,
  veryfiSeller,
  veryfiGrocer,
} from "../middlewares/auth";

import {
  add,
  query,
  list,
  update,
  remove,
  activate,
  desactivate,
  listClientes,
  listProveedores,
} from "../controllers/PersonaController";

const router = Router();

router.post("/add", veryfiUser, add);
router.get("/query", veryfiUser, query);
router.get("/list", veryfiUser, list);
router.put("/update", veryfiUser, update);
router.delete("/remove", veryfiUser, remove);
router.put("/activate", veryfiUser, activate);
router.put("/desactivate", veryfiUser, desactivate);

router.get("/listClientes", veryfiSeller, listClientes);
router.get("/listProveedores", veryfiGrocer, listProveedores);

export default router;
