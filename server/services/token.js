import jwt from "jsonwebtoken";
import Usuario from "../models/Usuario";

const checkToken = async (token) => {
  const { _id } = jwt.decode(token);

  if (!_id) {
    return;
  }

  const user = await Usuario.findOne({ _id, estado: 1 });

  if (user) {
    return jwt.sign({ _id }, "esteesuntoken", {
      expiresIn: "1d",
    });
  } else {
    return false;
  }
};

export const decode = async (token) => {
  try {
    const { _id } = jwt.verify(token, "esteesuntoken");
    const user = await Usuario.findOne({ _id, estado: 1 });
    !user && false;
    return user;
  } catch (error) {
    return await checkToken(token);
  }
};

export const encode = async (_id, rol, email, nombre, imagen) => {
  const token = jwt.sign({ _id, rol, email, nombre, imagen }, "esteesuntoken", {
    expiresIn: "1d",
  });
  return token
};
