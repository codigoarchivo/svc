import express from "express";
import morgan from "morgan";
import cors from "cors";
import path from "path";
import routes from "./routes";
import { port } from "./config";

const app = express();

app.use(cors());
app.use(morgan("dev"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));

app.use("/api", routes);

app.listen(port, () => {
  console.log(`Activado en el Puerto ${port}`);
});

export default app;