import { decode } from "../services/token";

export const veryfiUser = async (req, res, next) => {
  if (!req.headers.token) {
    return res.status(404).send({
      message: "No token",
    });
  }

  const { rol } = await decode(req.headers.token);

  if (rol === "Administrador" || rol === "Vendedor" || rol === "Almacenero") {
    next();
  } else {
    return res.status(403).send({
      message: "No Autorizado",
    });
  }
};

export const veryfiAdmin = async (req, res, next) => {
  if (!req.headers.token) {
    return res.status(404).send({
      message: "No token",
    });
  }

  const { rol } = await decode(req.headers.token);

  if (rol === "Administrador") {
    next();
  } else {
    return res.status(403).send({
      message: "No Autorizado",
    });
  }
};

export const veryfiGrocer = async (req, res, next) => {
  if (!req.headers.token) {
    return res.status(404).send({
      message: "No token",
    });
  }

  const { rol } = await decode(req.headers.token);

  if (rol === "Administrador" || rol === "Almacenero") {
    next();
  } else {
    return res.status(403).send({
      message: "No Autorizado",
    });
  }
};

export const veryfiSeller = async (req, res, next) => {
  if (!req.headers.token) {
    return res.status(404).send({
      message: "No token",
    });
  }

  const { rol } = await decode(req.headers.token);

  if (rol === "Administrador" || rol === "Vendedor") {
    next();
  } else {
    return res.status(403).send({
      message: "No Autorizado",
    });
  }
};
