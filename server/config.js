module.exports = {
  port: process.env.PORT || 3000,
  API_VERSION: "V1",
  IP_SERVER: "localhost",
  PORT_DB: 27017,
};
